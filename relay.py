#!/usr/bin/env python

import socket, requests, thread
from bluetooth import *
from flask import Flask, request

light_state = 'off'

def hostForHA(threadName, btSocket):
	
	app = Flask(__name__)
	
	
	@app.route("/light", methods=["GET", "POST"])
	def change_state():
		global light_state

		if request.method == "POST":
			light_state = request.get_json()['state']
			
			print "Changed state to", light_state
			
			try:
				btSocket.send(light_state)
			except:
				print 'Could not forward data to client'
				
			return light_state
		
			
				
		if request.method == "GET":
			print 'Responded to a GET request'
			return light_state

	
	
	app.run('0.0.0.0')
		


btPort = PORT_ANY
recvSize = 1024
uuid = "bfb7d366-c8ba-11e7-abc4-cec278b6b50a"
btSocket = BluetoothSocket(RFCOMM)
data = 'nothing yet'

url = 'http://jenkins.ovon.club:8123/api/services/btmsg_service/btmsg?api_password=group2'

btSocket.bind(('', btPort))
btSocket.listen(1)

try:
	advertise_service( btSocket, "Home Assistant Relay",
                   service_id = uuid,
                   service_classes = [ uuid, SERIAL_PORT_CLASS ],
                   profiles = [ SERIAL_PORT_PROFILE ])
	
except:
	print 'Could not advertise service'
try:	
	while True:

		print 'Awaiting bluetooth connection...'
	
	
		client, clientInfo = btSocket.accept()
		
		try:
			thread.start_new_thread(hostForHA, ('HA Entry Point', client, ))
		except:
			print 'Could not start HA entry thread'
			
		print 'Connection successful!'
		print 'Connected to: ', clientInfo
		print client
	
		while True:
			data = str(client.recv(recvSize))
		
			print data
			payload = {'name':data}
			print requests.post(url, json = payload)
except Exception as e:
	client.close()
	btSocket.close()
		
